# Belajar Git

- Clone repository ini
- Buat branch baru dengan nama masing-masing
- Jawab pertanyaan dibawah dengan merubah file README.md
- Commit perubahanmu
- Push branch ke remote environment

### Jawab pertanyaan dibawah ini


1. Apa yang kamu tau tentang _git_?
<!-- jawab disini -->

2. Sebutkan jenis-jenis _Version Control_ berdasarkan penerapannya?
<!-- jawab disini -->

3. Apa yang dimaksud dengan _branch_?
<!-- jawab disini -->

4. Command _pull_ berfungsi untuk apa?
<!-- jawab disini -->

5. Command _push_ berfungsi untuk apa?
<!-- jawab disini -->